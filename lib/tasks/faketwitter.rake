namespace :faketwitter do
  task :set_users_token => :environment do
    User.all.each do |user|
      user.set_token
      user.save
    end
  end
end

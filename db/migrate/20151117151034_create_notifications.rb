class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.integer :from_id
      t.integer :to_id
      t.boolean :read
      t.text :description

      t.timestamps null: false
    end
  end
end

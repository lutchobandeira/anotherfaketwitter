require 'faker'

FactoryGirl.define do
  factory :user, aliases: [:from, :to] do
    name { Faker::Name.name }
    username { Faker::Internet.user_name }
    email { Faker::Internet.email }
    password { Faker::Internet.password }
  end
end
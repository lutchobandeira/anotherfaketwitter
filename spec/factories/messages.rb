require 'faker'

FactoryGirl.define do
  factory :message do
    from
    to
    description { Faker::Lorem.paragraph }
  end
end

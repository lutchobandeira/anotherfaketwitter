require 'faker'

FactoryGirl.define do
  factory :post do
    user
    description { Faker::Lorem.characters(144) }
  end
  factory :long_post, class: Post do
    user
    description { Faker::Lorem.characters(145) }
  end
end

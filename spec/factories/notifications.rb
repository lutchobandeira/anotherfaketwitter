require 'faker'

FactoryGirl.define do
  factory :notification do
    from
    to
    read { false }
    description { Faker::Lorem.paragraph }
  end
end

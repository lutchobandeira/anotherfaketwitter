require 'rails_helper'

RSpec.describe 'Messages API' do
  
  describe 'GET /api/v1/messages' do
    it 'tries without a token' do
      get '/api/v1/messages'
      expect(response).to have_http_status(:unauthorized)
    end
    it 'gets all messages sent to and received by the calling user' do
      user = create(:user)
      getting_message = create(:message, to: user, description: "getting message")
      sending_message = create(:message, from: user, description: "sending message")
      another_message = create(:message)
      get "/api/v1/messages?token=#{user.token}"
      json = JSON.parse(response.body)
      expect(response).to be_success
      expect(json[0]["description"]).to eq("sending message")
      expect(json[1]["description"]).to eq("getting message")
    end
  end

  describe 'GET /api/v1/messages/:message_id' do
    it 'tries without a token' do
      message = create(:message)
      get "/api/v1/messages/#{message.id}"
      expect(response).to have_http_status(:unauthorized)
    end

    it 'gets message as the author' do
      user = create(:user)
      message = create(:message, from: user)
      get "/api/v1/messages/#{message.id}?token=#{user.token}"
      json = JSON.parse(response.body)
      expect(response).to be_success
      expect(json["id"]).to eq(message.id)
    end

    it 'gets message as the receiver' do
      user = create(:user)
      message = create(:message, to: user)
      get "/api/v1/messages/#{message.id}?token=#{user.token}"
      json = JSON.parse(response.body)
      expect(response).to be_success
      expect(json["id"]).to eq(message.id)
    end

    it 'tries to get a private message' do
      user = create(:user)
      message = create(:message)
      get "/api/v1/messages/#{message.id}?token=#{user.token}"
      expect(response).to have_http_status(:forbidden)
    end
  end

  describe 'DELETE /api/v1/messages/:message_id' do
    it 'tries without a token' do
      message = create(:message)
      delete "/api/v1/messages/#{message.id}"
      expect(response).to have_http_status(:unauthorized)
    end

    it 'tries to delete the message of someone else' do
      user = create(:user)
      another_user = create(:user)
      message = create(:message, from: another_user)
      delete "/api/v1/messages/#{message.id}?token=#{user.token}"
      expect(response).to have_http_status(:forbidden)
    end

    it 'deletes a message' do
      user = create(:user)
      another_user = create(:user)
      message = create(:message, from: user)
      delete "/api/v1/messages/#{message.id}?token=#{user.token}"
      expect(response).to be_success
      expect(Message.where(id: message.id)).not_to exist
    end
  end

end

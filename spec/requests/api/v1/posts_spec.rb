require 'rails_helper'

RSpec.describe 'Posts API' do
  
  describe 'GET /api/v1/posts' do
    context 'does not provide a token' do
      it 'retrieves the whole list of posts' do
        create_list(:post, 10)
        get '/api/v1/posts'
        json = JSON.parse(response.body)
        expect(response).to be_success
        expect(json.length).to eq(10)
      end
    end

    context 'provides a token' do
      it 'retrieves only the posts of who he follows' do
        create_list(:post, 10)

        user = create(:user)
        another_user = create(:user)
        user.follow(another_user)
        user.save

        post = create(:post, user: another_user)

        get "/api/v1/posts?token=#{user.token}"

        json = JSON.parse(response.body)
        expect(response).to be_success
        expect(json.length).to eq(1)
      end
    end
  end

  describe 'GET /api/v1/posts/:post_id' do
    it 'tries without a token' do
      post = create(:post)
      get "/api/v1/posts/#{post.id}"
      expect(response).to have_http_status(:unauthorized)
    end

    it 'tries with a valid id' do
      user = create(:user)
      post = create(:post)
      get "/api/v1/posts/#{post.id}?token=#{user.token}"
      json = JSON.parse(response.body)
      expect(response).to be_success
      expect(json["id"]).to eq(post.id)
    end
  end

  describe 'DELETE /api/v1/posts/:post_id' do
    it 'tries without a token' do
      post = create(:post)
      delete "/api/v1/posts/#{post.id}"
      expect(response).to have_http_status(:unauthorized)
    end

    it 'tries to delete the post of someone else' do
      user = create(:user)
      another_user = create(:user)
      post = create(:post, user: another_user)
      delete "/api/v1/posts/#{post.id}?token=#{user.token}"
      expect(response).to have_http_status(:forbidden)
    end
  end

end

require 'rails_helper'

RSpec.describe 'Users API' do
  
  describe 'GET /api/v1/users' do
    it 'tries without a token' do
      get '/api/v1/users'
      json = JSON.parse(response.body)
      expect(response).to have_http_status(:unauthorized)
    end

    it 'gets all users' do
      users = create_list(:user, 10)
      get "/api/v1/users?token=#{users[0].token}"
      json = JSON.parse(response.body)
      expect(response).to be_success
      expect(json.length).to eq(10)
    end

    it 'searches for an user' do
      create(:user, username: 'anotheruser')
      user = create(:user, username: 'lutchobandeira')
      get "/api/v1/users?token=#{user.token}&q=lutcho"
      json = JSON.parse(response.body)
      expect(response).to be_success
      expect(json.length).to eq(1)
      expect(json[0]["username"]).to eq('lutchobandeira')
    end
  end

  describe 'PUT /api/v1/users/follow' do
    it 'tries without a token' do
      put '/api/v1/users/1/follow'
      json = JSON.parse(response.body)
      expect(response).to have_http_status(:unauthorized)
    end

    it 'follows another user' do
      user = create(:user)
      another_user = create(:user)
      put "/api/v1/users/#{another_user.id}/follow?token=#{user.token}"
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'PUT /api/v1/users/unfollow' do
    it 'tries without a token' do
      put '/api/v1/users/1/follow'
      json = JSON.parse(response.body)
      expect(response).to have_http_status(:unauthorized)
    end
    
    it 'unfollows another user' do
      user = create(:user)
      another_user = create(:user)
      put "/api/v1/users/#{another_user.id}/unfollow?token=#{user.token}"
      expect(response).to have_http_status(:ok)
    end
  end

  describe 'GET /api/v1/users' do
    it 'tries without a token' do
      user = create(:user)
      another_user = create(:user)
      get "/api/v1/users/#{another_user.id}"
      json = JSON.parse(response.body)
      expect(response).to have_http_status(:unauthorized)
    end

    it 'gets user' do
      users = create_list(:user, 10)
      another_user = create(:user)
      get "/api/v1/users/#{another_user.id}?token=#{users[0].token}"
      json = JSON.parse(response.body)
      expect(response).to be_success
      expect(json["username"]).to eq(another_user.username)
    end
  end
end
require 'rails_helper'

RSpec.describe Notification, type: :model do
  
  it 'has a valid factory' do
    notification = build(:notification)
    notification.read = false
    notification.should be_valid
  end

  it 'is invalid without a origin user' do
    notification = build(:notification, from: nil)
    notification.should be_invalid
  end
  
  it 'is invalid without a destination user' do
    notification = build(:notification, to: nil)
    notification.should be_invalid
  end

  it 'is invalid without a destination description' do
    notification = build(:notification, description: nil)
    notification.should be_invalid
  end

end

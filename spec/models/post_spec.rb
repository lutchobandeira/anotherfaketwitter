require 'rails_helper'

RSpec.describe Post, type: :model do
  
  it 'has a valid factory' do
    build(:post).should be_valid
  end

  it 'is invalid without an user' do
    build(:post, user: nil).should be_invalid
  end

  it 'is invalid without a description' do
    build(:post, description: nil).should be_invalid
  end

  it 'is invalid when description is more than 144 characters long' do
    build(:long_post).should be_invalid
  end

end

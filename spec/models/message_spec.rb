require 'rails_helper'

RSpec.describe Message, type: :model do
  
  it 'has a valid factory' do
    message = build(:message)
    message.should be_valid
  end

  it 'is invalid without a origin user' do
    message = build(:message, from: nil)
    message.should be_invalid
  end
  
  it 'is invalid without a destination user' do
    message = build(:message, to: nil)
    message.should be_invalid
  end

  it 'is invalid without a destination description' do
    message = build(:message, description: nil)
    message.should be_invalid
  end

end

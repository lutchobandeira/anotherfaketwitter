require 'rails_helper'

RSpec.describe User, type: :model do
  
  it 'has a valid factory' do
    build(:user).should be_valid
  end
  
  it 'is invalid without a name' do
    build(:user, name: nil).should be_invalid
  end

  it 'is invalid without an username' do
    build(:user, username: nil).should be_invalid
  end

  it 'is invalid with a duplicated username' do
    create(:user, username: 'username')
    build(:user, username: 'username').should be_invalid
  end

  it 'is invalid with a invalid username' do
    build(:user, username: 'user name').should be_invalid
  end

  it 'is invalid without an email' do
    build(:user, email: nil).should be_invalid
  end

  it 'is invalid with a invalid email' do
    build(:user, email: 'email@').should be_invalid
  end

  it 'is invalid with a duplicated email' do
    create(:user, email: 'email@email.com')
    build(:user, email: 'email@email.com').should be_invalid
  end

  it 'is invalid without a password' do
    build(:user, password: nil).should be_invalid
  end

  it 'starts to follow another user' do
    user = create(:user)
    another_user = create(:user)
    user.follow(another_user)
    another_user.followers.should include(user)
  end

  it 'unfollows another user' do
    user = create(:user)
    another_user = create(:user)
    user.unfollow(another_user)
    another_user.followers.should_not include(user)
  end

  it 'is following another user' do
    user = create(:user)
    another_user = create(:user)
    another_user.followers << user
    user.follows?(another_user).should be true
  end
  
end

require 'rails_helper'

RSpec.describe PostsController, type: :controller do
  
  describe 'GET #index' do
    context 'user signed in' do
      it 'display posts of followees' do
        # create users
        user = create(:user)
        followee = create(:user)
        no_followee = create(:user)
        user.followees << followee

        # create posts
        post = build(:post)
        post.user = followee
        post.save

        post2 = build(:post)
        post2.user = no_followee
        post2.save

        # populate users with posts
        followee.posts << post
        no_followee.posts << post2

        sign_in user

        get :index
        assigns(:posts).should eq([post])
      end
      it 'assigns new post associated with the current user to @post' do
        user = create(:user)
        post = build(:post, user: user)
        sign_in user 
        get :index
        assigns(:post).should eq(post)
      end
    end
    
    context 'user it not signed in' do
      it 'display all posts' do
        # create users
        user = create(:user)
        followee = create(:user)
        no_followee = create(:user)
        user.followees << followee

        # create posts
        post = build(:post)
        post.user = followee
        post.save

        post2 = build(:post)
        post2.user = no_followee
        post2.save

        # populate users with posts
        followee.posts << post
        no_followee.posts << post2

        get :index
        assigns(:posts).should eq([post2, post])
      end
    end

    it 'renders the :index view' do
      get :index
      response.should render_template :index
    end
  end

  describe 'GET #show' do
    it 'assigns the requested post to @post' do
      sign_in create(:user)
      post = create(:post)
      puts post.inspect
      get :show, id: post
      assigns(:post).should eq(post)
    end
    
    it 'renders the :show view' do
      sign_in create(:user)
      get :show, id: create(:post)
      response.should render_template :show
    end
  end

  describe 'POST #create' do
    context 'with valid attributes' do
      it 'creates a new post' do
        sign_in create(:user)
        expect{
          post :create, post: attributes_for(:post)
        }.to change(Post,:count).by(1)
      end
      
      it 'redirects to timeline' do
        sign_in create(:user)
        post :create, post: attributes_for(:post)
        response.should redirect_to root_path
      end
    end
    
    context 'with invalid attributes' do
      it 'does not save the new post' do
        sign_in create(:user)
        expect{
          post :create, post: attributes_for(:post, description: nil)
        }.to_not change(Post, :count)
      end
      
      it 're-renders the timeline' do
        sign_in create(:user)
        post :create, post: attributes_for(:post, description: nil)
        response.should render_template :index
      end
    end 
  end

  describe 'DELETE #destroy' do
    before :each do
      user = create(:user)
      sign_in user
      @post = create(:post, user: user)
    end
    
    it 'deletes the post' do
      expect{
        delete :destroy, id: @post
      }.to change(Post, :count).by(-1)
    end
      
    it 'redirects to timeline' do
      delete :destroy, id: @post
      response.should redirect_to root_path
    end
  end

end

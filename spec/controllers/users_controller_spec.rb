require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  
  describe 'GET #index' do
    it 'display all users' do
      another_user = create(:user, username: 'bbb')
      user = create(:user, username: 'aaa')
      sign_in user
      get :index
      assigns(:users).should eq([user, another_user])
    end  

    it 'renders the :index view' do
      sign_in create(:user)
      get :index
      response.should render_template :index
    end
  end

end

require 'rails_helper'

RSpec.describe NotificationsController, type: :controller do

  describe 'PUT #update' do
    before :each do
      @user = create(:user)
      sign_in @user
      @notification = create(:notification, from: @user)
    end
      
    it 'redirects to the origin user' do
      put :update, id: @notification, notification: attributes_for(:notification)
      response.should redirect_to @user
    end
  end

end

require 'rails_helper'

RSpec.describe MessagesController, type: :controller do
  
  describe 'GET #index' do
    it 'display all messages sent to and received by current user' do
      user = create(:user)
      message_to_user = create(:message, to: user)
      message_from_user = create(:message, from: user)
      private_message = create(:message)
      sign_in user
      get :index
      assigns(:messages).should eq([message_from_user, message_to_user])
    end  

    it 'renders the :index view' do
      sign_in create(:user)
      get :index
      response.should render_template :index
    end
  end

  describe 'DELETE #destroy' do
    before :each do
      user = create(:user)
      sign_in user
      @message = create(:message, to: user)
    end
    
    context 'current user is the receiver of the message' do
      it 'deletes the message' do
        expect{
          delete :destroy, id: @message
        }.to change(Message, :count).by(-1)
      end
    end
  end

end

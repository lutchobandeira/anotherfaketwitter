require 'rails_helper'

RSpec.describe "Users", :type => :feature do
  
  describe 'GET /' do
    
    context 'user signed in' do
      it 'sees his/her profile' do
        user = FactoryGirl.create(:user)
        login_as(user, :scope => :user)
        visit root_url
        page.should have_content user.name
        page.should have_content user.username
      end
    end
    
    context 'user is a visitor' do
      it 'sees his/her profile' do
        visit root_url
        page.should have_content "Welcome to Fake Twitter"
      end
    end

  end

end

class MessagesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_message, only: [:destroy]

  # GET /messages
  # GET /messages.json
  def index
    @messages = Message.chat(current_user).page(params[:page])
  end

  # POST /messages
  # POST /messages.json
  def create
    @user = User.find(message_params[:to_id])
    @message = Message.new(message_params)
    @message.from = current_user
    @message.to = @user

    respond_to do |format|
      if @message.save
        format.html { redirect_to @user, notice: 'Message was successfully sent.' }
        format.json { render :show, status: :created, location: @message }
      else
        format.html { render "users/show" }
        format.json { render json: @message.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /messages/1
  # DELETE /messages/1.json
  def destroy
    @message.destroy if @message.to == current_user
    respond_to do |format|
      format.html { redirect_to messages_url, notice: 'Message was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_message
      @message = Message.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def message_params
      params.require(:message).permit(:description, :to_id)
    end
end

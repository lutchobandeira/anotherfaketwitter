class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user, only: [:show, :follow, :update, :unfollow, :destroy]

  # GET /users
  # GET /users.json
  def index
    @q = User.ransack(params[:q])
    @users = @q.result.page(params[:page]).order(:username)
    @message = Message.new
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @message = Message.new
    @message.to = @user
  end

  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to :back, notice: 'Photo updated successfully.' }
        format.json { render :show, status: :ok, location: @flag }
      else
        @post = Post.new
        @posts = Post.timeline(current_user).page(params[:page])
        @user_avatar = @user
        format.html { render "posts/index" }
        format.json { render json: @flag.errors, status: :unprocessable_entity }
      end
    end
  end

  def follow
    respond_to do |format|
      if @user.id == current_user.id
        format.html { redirect_to @user, notice: 'You cannot follow yourself.' }
        format.json { render :show, status: :ok, location: @user }
      elsif current_user.follow(@user)
        format.html { redirect_to @user, notice: "You are following @#{@user.username}." }
        format.json { render :show, status: :ok, location: @user }
      end
    end
  end

  def unfollow
    respond_to do |format|
      if @user.id == current_user.id
        format.html { redirect_to @user, notice: "You cannot unfollow yourself" }
        format.json { render :show, status: :ok, location: @user }
      elsif current_user.unfollow(@user)
        format.html { redirect_to @user, notice: "You unfollowed @#{@user.username}." }
        format.json { render :show, status: :ok, location: @user }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:name, :username, :avatar)
    end
end

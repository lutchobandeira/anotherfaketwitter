class Api::V1::PostsController < ApplicationController
  respond_to :json
  skip_before_filter :verify_authenticity_token

  def index
    user = User.find_by(token: params[:token])
    if user.nil?
      posts = Post.order(id: :desc)
    else
      posts = Post.timeline(user)
    end
    render json: posts.to_json, status: :ok
  end

  def fresh
    user = User.find_by(token: params[:token])
    puts params.inspect
    if !params.has_key?(:after)
      message = { message: 'Required parameter *after* was not passed.' }
      render json: message.to_json, status: :bad_request
      return  
    elsif user.nil?
      posts = Post.timeline(current_user).where("posts.id > ?", params[:after].to_i)
    else
      posts = Post.where("id > ?", params[:after].to_i).order(id: :desc)
    end
    render json: posts.to_json, status: :ok
  end

  def show
    user = User.find_by(token: params[:token])
    if user.nil?
      message = { message: 'User not found' }
      render json: message.to_json, status: :unauthorized
      return
    else
      post = Post.find(params[:id])
    end
    render json: post.to_json, status: :ok
  end

  def destroy
    user = User.find_by(token: params[:token])
    if user.nil?
      message = { message: 'User not found' }
      render json: message.to_json, status: :unauthorized
      return
    else
      post = Post.find(params[:id])
      if post.user.id == user.id && post.destroy
        render json: post.to_json, status: :ok
      else
        render json: message.to_json, status: :forbidden
      end
    end
  end  

end
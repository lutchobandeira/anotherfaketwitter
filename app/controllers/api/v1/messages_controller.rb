class Api::V1::MessagesController < ApplicationController
  respond_to :json
  skip_before_filter :verify_authenticity_token

  def index
    user = User.find_by(token: params[:token])
    if user.nil?
      message = { message: 'User not found' }
      render json: message.to_json, status: :unauthorized
      return
    else
      messages = Message.chat(user)
      render json: messages.to_json, status: :ok
    end
  end

  def show
    user = User.find_by(token: params[:token])
    if user.nil?
      result = { message: 'User not found' }
      render json: result.to_json, status: :unauthorized
      return
    else
      message = Message.find(params[:id])
      if message.from_id == user.id || message.to_id == user.id
        render json: message.to_json, status: :ok
        return
      else
        result = { message: 'Forbidden to view messages not sent to or received by the user.' }
        render json: result.to_json, status: :forbidden
      end
    end
  end

  def destroy
    user = User.find_by(token: params[:token])
    if user.nil?
      result = { message: 'User not found' }
      render json: result.to_json, status: :unauthorized
    else
      message = Message.find(params[:id])
      if message.from_id == user.id
        message.destroy
        render json: message.to_json, status: :ok
      else
        result = { message: 'Forbidden to delete messages from someone else' }
        render json: result.to_json, status: :forbidden
      end
    end
  end  

end
class Api::V1::UsersController < ApplicationController
  respond_to :json
  skip_before_filter :verify_authenticity_token

  def index
    user = User.find_by(token: params[:token])
    if user.nil?
      message = { message: 'User not found' }
      render json: message.to_json, status: :unauthorized
      return
    elsif params.has_key?(:q)
      search_params = {}
      search_params[:name_or_username_or_email_cont] = params[:q]
      users = User.ransack(search_params).result.order(:username)
    else
      users = User.all
    end
    render json: users.to_json, status: :ok
  end

  def follow
    user = User.find_by(token: params[:token])
    if user.nil?
      message = { message: 'User not found' }
      render json: message.to_json, status: :unauthorized
      return  
    else
      another_user = User.find(params[:id])
      if another_user.nil?
        message = { message: 'Folowee not found.' }
        render json: message.to_json, status: :bad_request
      else
        user.follow(another_user)
        render json: user.to_json, status: :ok
      end
    end
  end

  def unfollow
    user = User.find_by(token: params[:token])
    if user.nil?
      message = { message: 'User not found' }
      render json: message.to_json, status: :unauthorized
      return  
    else
      another_user = User.find(params[:id])
      if another_user.nil?
        message = { message: 'Folowee not found.' }
        render json: message.to_json, status: :bad_request
      else
        user.unfollow(another_user)
        render json: user.to_json, status: :ok
      end
    end
  end

  def show
    user = User.find_by(token: params[:token])
    if user.nil?
      message = { message: 'User not found' }
      render json: message.to_json, status: :unauthorized
      return
    else
      found_user = User.find(params[:id])
    end
    render json: found_user.to_json, status: :ok
  end

end
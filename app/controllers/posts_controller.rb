class PostsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :fresh]
  before_action :set_post, only: [:show, :destroy]

  # GET /posts
  # GET /posts.json
  def index
    if user_signed_in?
      @post = Post.new
      @posts = Post.timeline(current_user).page(params[:page])
    else
      @posts = Post.page(params[:page]).order(id: :desc)
    end
  end

  def fresh
    if user_signed_in?
      @posts = Post.timeline(current_user).where("posts.id > ?", params[:after].to_i)
    else
      @posts = Post.where("id > ?", params[:after].to_i).order(id: :desc)
    end
    respond_to do |format|
      format.js {render layout: false}
    end
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(post_params)
    @post.user = current_user
    respond_to do |format|
      if @post.save
        format.html { redirect_to root_url, notice: 'Post was successfully created.' }
        format.json { render :show, status: :created, location: @post }
      else
        @posts = Post.timeline(current_user).page(params[:page])
        format.html { render :index }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    respond_to do |format|
      if @post.user.id == current_user.id && @post.destroy
        format.html { redirect_to root_url, notice: 'Post was successfully destroyed.' }
        format.json { head :no_content }
      else
        format.html { redirect_to root_url, notice: 'Post was not destroyed.' }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:user_id, :description)
    end
end

class NotificationsController < ApplicationController

  before_action :authenticate_user!

  def update
    @notification = Notification.find(params[:id])
    @notification.update(notification_params)
    respond_to do |format|
      format.html { redirect_to @notification.from }
    end
  end

  def fresh
    @notifications = current_user.notifications.unread.where("notifications.id > ?", params[:after].to_i)
    @count = current_user.notifications.unread.count
    respond_to do |format|
      format.js {render layout: false}
    end
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def notification_params
      params.require(:notification).permit(:description, :to_id, :read)
    end

end

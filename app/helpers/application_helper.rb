module ApplicationHelper
  def active_class(controller_name, action_name)
    if controller_name == params[:controller] && action_name == params[:action]
      "active"
    else
      ""
    end
  end

  def hide_when(expression_true)
    logger.debug "expression_true: #{expression_true.inspect}"
    if expression_true
      "hide"
    else
      ""
    end
  end
end

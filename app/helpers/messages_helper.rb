module MessagesHelper
  def display_author(user)
    if user == current_user
      "you"
    else
      "@#{user.username}"
    end
  end
end

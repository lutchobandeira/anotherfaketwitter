# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).ready ->
  if $("#message_modal .has-error").length > 0
    $("#message_modal").modal()
  
->
  $('#message_modal').on 'show.bs.modal', (event) ->
    button = $(event.relatedTarget)
    name = button.data('name')
    to_id = button.data('to_id')
    modal = $(this)
    modal.find('.modal-title').text("New message to #{name}")
    $('#message_to_id').val(to_id)
    $('#message_description')[0].focus()

jQuery ->
  $("#user_username").keyup ->
    username = $(this).val()
    console.log(username)
    $("#avatar_sign_up").attr("src", "http://api.adorable.io/avatars/200/#{username}")
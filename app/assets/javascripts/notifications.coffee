# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

@request_notification = ->
  $.get($("#notifications").data("url"), after: $(".notification").first().data("id"))

@poll_notification = ->
  setTimeout request_notification, 5000

@addNotifications = (notifications) ->
  if notifications.length > 0
    $("#notifications").prepend($(notifications))
  poll_notification()

jQuery ->
    poll_notification()
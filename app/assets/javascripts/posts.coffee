# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

@request = ->
  $.get($("#posts").data("url"), after: $(".post").first().data("id"))

@poll = ->
  setTimeout request, 5000

@addPosts = (posts) ->
  if posts.length > 0
    $("#posts").prepend($(posts).hide())
    $("#show_posts").show()
  poll()

jQuery ->
  if $("#posts").length > 0
    poll()
    $("#show_posts a").click (e) ->
      e.preventDefault()
      $(".post").show()
      $("#show_posts").hide()

  $("#post_description").keyup ->
    length = $(this).val().length
    remaining = 144 - length
    $("#new_post .post_description p.help-block").text(remaining)

    if remaining < 0
      $("#new_post .post_description").addClass("has-error")
    else
      $("#new_post .post_description").removeClass("has-error")

$(document).ready ->
  if $("#photo .has-error").length > 0
    $("#photo").modal()
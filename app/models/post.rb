class Post < ActiveRecord::Base
  belongs_to :user

  validates :user, :description, presence: true
  validates :description, length: { maximum: 144 }

  self.per_page = 20

  scope :timeline, ->(user) { Post.joins("LEFT JOIN friendships ON friendships.followee_id = posts.user_id")
                                  .joins("LEFT JOIN users ON users.id = friendships.follower_id")
                                  .where("users.id = ? OR posts.user_id = ?", user.id, user.id)
                                  .order("posts.id DESC") }

  def ==(another_post)
    id == another_post.id
  end

end

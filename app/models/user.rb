class User < ActiveRecord::Base
  
  before_create :set_token

  has_many :follower_friendships, foreign_key: :followee_id, class_name: "Friendship"
  has_many :followers, through: :follower_friendships, source: :follower

  has_many :followee_friendships, foreign_key: :follower_id, class_name: "Friendship"
  has_many :followees, through: :followee_friendships, source: :followee


  has_many :messages, foreign_key: :to_id
  has_many :notifications, foreign_key: :to_id
  has_many :posts

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :name, presence: true
  validates :username, format: { with: /\A(\w|\.)+\Z/ }, uniqueness: true, presence: true
  validates :email, email: true, uniqueness: true, presence: true

  self.per_page = 20
  
  has_attached_file :avatar, styles: {
    thumb: '100x100>',
    square: '200x200#',
    medium: '300x300>'
  }

  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  
  def placeholder_avatar(size=200)
    "http://api.adorable.io/avatars/#{size}/#{username}"
  end

  def follow(another_user)
    another_user.followers << self
    notification = Notification.new
    notification.to = another_user
    notification.from = self
    notification.description = "You have a new follower: @#{self.username}."
    notification.read = false
    notification.save
  end

  def unfollow(another_user)
    another_user.followers.delete(self)
  end

  def follows?(another_user)
    followees.where(id: another_user.id).exists?
  end

  def set_token
    return if token.present?
    self.token = generate_token
  end

  def generate_token
    SecureRandom.uuid.gsub(/\-/,'')
  end

  def to_s
    username
  end
end

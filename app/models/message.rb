class Message < ActiveRecord::Base
  belongs_to :from, foreign_key: "from_id", class_name: "User"
  belongs_to :to, foreign_key: "to_id", class_name: "User"

  validates :from, :to, :description, presence: true

  self.per_page = 20

  scope :chat, ->(user) { Message.where("messages.from_id = ? OR messages.to_id = ?", user.id, user.id)
                                     .order(created_at: :desc) }
  
end

class Notification < ActiveRecord::Base
  belongs_to :from, foreign_key: "from_id", class_name: "User"
  belongs_to :to, foreign_key: "to_id", class_name: "User"

  validates :from, :to, :description, presence: true

  scope :unread, -> { where(read: false).order(id: :desc) }
end
